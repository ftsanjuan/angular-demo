module.exports = function(grunt) {
  grunt.initConfig({
    concat: {
      js: {
        src: ['app/app.js', 'app/js/**/**.js'],
        // Compile to a single file to add a script tag for in your HTML
        dest: 'tmp/app.js'
      }
    },
    uglify: {
      options: {
        mangle: false,
        uglify: true
      },
      default: {
        files: {
          "app.min.js": ["tmp/app.js"]
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      files: ['app/**/*'],
      tasks: ['concat:js', 'uglify'],
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // The default tasks to run when you type: grunt
  grunt.registerTask('default', ['watch']);
};